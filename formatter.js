javascript: (function() {
  var getFieldValue = function(fieldName) {
    // selector works on kibana permalink log page
    var selector = "doc-viewer table td[title='%FIELD%'] + td > div".replace("%FIELD%", fieldName);
    var elem = $(selector);
    if(elem.length > 0) return elem.text();
  };
  var formatSql = function(query, params) {
    if (!query || !jsonParams) throw 'Sql or SqlParameters cannot be empty';

    _.forEach(params, function(p) {
      console.log("replacing " + p.Name + " with " + p.Value);
      query = query.replace(": " + p.Name, " " + formatParamValueByType(p.Value));
      query = query.replace(":" + p.Name, " " + formatParamValueByType(p.Value));
    });

    return query;
  };
  var matchDate = function(input) {
    var iso = /^(\d{4}\-\d\d\-\d\d([tT][\d:\.]*)?)([zZ]|([+\-])(\d\d):(\d\d))?$/;
    return input.match(iso);
  };
  var formatParamValueByType = function(obj) {
    var number = +obj;
    if (number) return number;

    if (matchDate(obj)) return "to_timestamp('%DATE', 'YYYY-MM-DD\"T\"HH24:MI:SS.FF7\"Z\"')".replace("%DATE", obj);
    return "'" + obj + "'";
  };
  
  try {
    var sql = getFieldValue('Sql');
    var jsonParams = getFieldValue('SqlParamaters');

    console.log(sql);
    console.log(jsonParams);
    
    var params = JSON.parse("[" + jsonParams + "]");
    alert(formatSql(sql, params));
  } catch (err) {
    alert("Bookmarklet fatal error. " + err.message);
  }
})();